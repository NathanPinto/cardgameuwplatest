﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
	public struct GameScore
	{
		//Declare the variables that represent the score
		private int _userScore;

		private int _houseScore;

		public GameScore(int userScore, int houseScore)
		{
			_userScore = userScore;
			_houseScore = houseScore;
			
		}
	}

	public class CardGame
	{
		//Declare the card deck
		private CardDeck _cardDeck;

		//Declare the score for the game
		private GameScore _score;

		//declare the current card played by the house
		private Card _houseCard;

		//declare the current card player by the player
		private Card _playerCard;

		public CardGame()
		{
			_cardDeck = new CardDeck();

			_score = new GameScore(0, 0);

			_playerCard = null;

			_houseCard = null;

		}

		public void Play()
		{ 
		}

		private byte DetermineCardRank(Card card)
		{
			return 0;
		}
	}
}
